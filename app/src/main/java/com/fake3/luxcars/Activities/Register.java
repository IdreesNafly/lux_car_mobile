package com.fake3.luxcars.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fake3.luxcars.Models.User;
import com.fake3.luxcars.R;
import com.fake3.luxcars.Services.RetrofitService;
import com.fake3.luxcars.Services.UserService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Register extends AppCompatActivity {
Button Register;
TextView Login;
ImageView back;
EditText FullName,Email,Number,Password,ConfirmPassword;
String fullName,email,password,confirmPassword;
String number;
    TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        FullName=findViewById(R.id.name);
        Email=findViewById(R.id.email);
        back=findViewById(R.id.backicon);
        Number=findViewById(R.id.number);
        Password=findViewById(R.id.password);
        ConfirmPassword=findViewById(R.id.confirmPassword);
        Register=findViewById(R.id.register);
        Login=findViewById(R.id.login);
        title=findViewById(R.id.title);
        title.setText("Register");

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                previous();
            }
        });


        Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initialize();
                if(Validate()) {
                    User user = new User(email, fullName, number, password);
                    retrofit2.Retrofit retrofit = RetrofitService.getRetrofit();
                    UserService userService = retrofit.create(UserService.class);
                    Call<User> pp = userService.registerUser(user);
                    pp.enqueue(new Callback<User>() {
                        @Override
                        public void onResponse(Call<User> call, Response<User> response) {
                            User r = response.body();
                            if (r != null) {
                                Toast.makeText(getApplicationContext(), "User successfully created", Toast.LENGTH_LONG).show();

                            }
                        }

                        @Override
                        public void onFailure(Call<User> call, Throwable thtow) {
                        }
                    });
                }
            }
        });


    }

    public void previous(){
        super.onBackPressed();
    }


    public Boolean Validate(){
        Boolean flag=true;
         if(TextUtils.isEmpty(email)){
            Email.setError("Email cannot be empty");
            flag=false;
        }
         else if(TextUtils.isEmpty(fullName)){
             FullName.setError("Fullname cannot be empty");
             flag=false;
         }
         else if(TextUtils.isEmpty(password)){
             Password.setError("Password cannot be empty");
             flag=false;
         }
         else if(TextUtils.isEmpty(String.valueOf(number))){
             Number.setError("Number cannot be empty");
             flag=false;
         }
         else if(TextUtils.isEmpty(confirmPassword)){
             ConfirmPassword.setError("Confirm Password cannot be empty");
             flag=false;
         }
        else if (!Patterns.EMAIL_ADDRESS.matcher(email.toString()).matches()){
             Email.setError("Email not valid");
             flag=false;
        }
         return flag;

    }

    public void initialize(){
        fullName=FullName.getText().toString();
        email=Email.getText().toString();
        password=Password.getText().toString();
        confirmPassword=ConfirmPassword.getText().toString();
        number=Number.getText().toString();
    }
}
