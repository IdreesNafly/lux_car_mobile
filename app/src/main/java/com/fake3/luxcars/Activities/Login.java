package com.fake3.luxcars.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fake3.luxcars.Models.JwtResponse;
import com.fake3.luxcars.Models.SignInRequest;
import com.fake3.luxcars.R;
import com.fake3.luxcars.Services.RetrofitService;
import com.fake3.luxcars.Services.UserService;
import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity implements View.OnClickListener{
    ImageView back;
    TextView title,signup;
    Button signin;
    Long id;
    //EditText email,password;
    String Email,Password;
    TextInputEditText email,password;
    String value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Intent intent=getIntent();
        value=intent.getStringExtra("sample");
        id=intent.getLongExtra("id",0);
        back=findViewById(R.id.backicon);
        signin=findViewById(R.id.signIn);
        signup=findViewById(R.id.signup);
        signin.setOnClickListener(this);
        signup.setOnClickListener(this);
        title=findViewById(R.id.title);
        email=findViewById(R.id.username);
        password=findViewById(R.id.user_password);
        title.setText("Login");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    previous();
            }
        });
    }


    public  boolean validate(){
        Email=email.getText().toString();
        Password=password.getText().toString();
        boolean flag=true;
        if(TextUtils.isEmpty(Email)){
            email.setError("Please Enter Email");
            flag=false;
        }
        else if(TextUtils.isEmpty(Password)){
            password.setError("Please Enter Password");
            flag=false;
        }  else if (Patterns.EMAIL_ADDRESS.matcher(email.toString()).matches()){
            email.setError("Email not valid");
            flag=false;
        }
     return flag;
    }


    @Override
    public void onClick(View v) {

        switch(v.getId()){

            case R.id.signIn: /** Start a new Activity MyCards.java */
            if(validate()) {
                retrofit2.Retrofit retrofit = RetrofitService.getRetrofit();
                UserService userService = retrofit.create(UserService.class);
                SignInRequest user = new SignInRequest(Email, Password);
                Call<JwtResponse> pp = userService.loginUser(user);
                pp.enqueue(new Callback<JwtResponse>() {
                    @Override
                    public void onResponse(Call<JwtResponse> call, Response<JwtResponse> response) {
                        JwtResponse j = (JwtResponse) response.body();
                        if (j != null) {
                            SharedPreferences.Editor editor = getSharedPreferences("shared", MODE_PRIVATE).edit();
                            editor.putString("email",j.getEmail());
                            editor.putString("token",j.getType()+" "+j.getToken());
                            editor.putString("admin",j.getRoles().get(0));
                            editor.apply();
                            List<String> roles=j.getRoles();
                            if(roles.get(0).equals("ROLE_ADMIN")){
                                Intent intent = new Intent(getApplicationContext(), AdminActivity.class);
                                intent.putExtra("command", "profile");
                                startActivity(intent);
                                finish();
                            }
                            else {
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                intent.putExtra("command", "profile");
                                startActivity(intent);
                                previous();
                            }
                        }
                        else {
                            Toast.makeText(Login.this, "User Credintials Wrong", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<JwtResponse> call, Throwable thtow) {
                        if (thtow instanceof IOException) {
                            Toast.makeText(Login.this, "this is an actual network failure :( inform the user and possibly retry", Toast.LENGTH_SHORT).show();
                            // logging probably not necessary
                        } else {
                            Toast.makeText(Login.this, "conversion issue! big problems :(", Toast.LENGTH_SHORT).show();
                            // todo log to some central bug tracking service
                        }

                    }
                });
            }
                 break;

            case R.id.signup:
               previous();
                break;
        }
    }

    public void previous(){
        if(value.equals("login")){
            Intent intent1=new Intent(getApplicationContext(),MainActivity.class);
            startActivity(intent1);
        }
       else if(value.equals("detail")){

            Intent intent=new Intent( Login.this, DetailProduct.class);
            intent.putExtra("id",id);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        else {
            super.onBackPressed();
        }
    }

}
