package com.fake3.luxcars.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fake3.luxcars.Adapter.SaveListAdapter;
import com.fake3.luxcars.Adapter.VehicleAdapter;
import com.fake3.luxcars.Models.SaveList;
import com.fake3.luxcars.Models.Vehicle;
import com.fake3.luxcars.R;
import com.fake3.luxcars.Services.RetrofitService;
import com.fake3.luxcars.Services.SaveListService;
import com.fake3.luxcars.Services.VehicleService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SaveListActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    VehicleAdapter vehicleAdapter;
    List<SaveList> saveList;
    ImageView back;
    TextView title;

    public void previous(){
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_list);
        recyclerView=findViewById(R.id.recycleView);
        back=findViewById(R.id.backicon);
        title=findViewById(R.id.title);
        title.setText("Save List");
        recyclerView.setHasFixedSize(true);
        SharedPreferences prefs = getSharedPreferences("shared",MODE_PRIVATE);
        String email=  prefs.getString("email", "");
        String token=prefs.getString("token","");
        recyclerView.setLayoutManager(new GridLayoutManager(this,2));
        saveList=new ArrayList<>();

        retrofit2.Retrofit retrofit= RetrofitService.getRetrofit();
        SaveListService saveListService=retrofit.create(SaveListService.class);
        Call<List<SaveList>> pp=saveListService.getSaveList(email,token);
        pp.enqueue(new Callback<List<SaveList>>() {
            @Override
            public void onResponse(Call<List<SaveList>> call, Response<List<SaveList>> response) {
                saveList = response.body();
                SaveListAdapter saveListAdapter=new SaveListAdapter(getApplicationContext(),saveList);
                recyclerView.setAdapter(saveListAdapter);
                saveListAdapter.notifyDataSetChanged();
            }
            @Override
            public void onFailure(Call<List<SaveList>> call, Throwable t) {

            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                previous();
            }
        });
    }
}
