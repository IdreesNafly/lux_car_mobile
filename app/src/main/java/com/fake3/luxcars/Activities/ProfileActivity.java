package com.fake3.luxcars.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fake3.luxcars.Adapter.VehicleAdapter;
import com.fake3.luxcars.Fragments.Home;
import com.fake3.luxcars.Fragments.Profile;
import com.fake3.luxcars.Models.User;
import com.fake3.luxcars.Models.Vehicle;
import com.fake3.luxcars.R;
import com.fake3.luxcars.Services.RetrofitService;
import com.fake3.luxcars.Services.UserService;
import com.fake3.luxcars.Services.VehicleService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {
//ImageView back;
TextView name,email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_profile);
//        back=findViewById(R.id.backicon);
//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//               previous();
//
//            }
//        });
       name=findViewById(R.id.name);
        email=findViewById(R.id.email);
        SharedPreferences prefs = getSharedPreferences("shared",MODE_PRIVATE);
      //  SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String userEmail=  prefs.getString("email", "");
        String token=prefs.getString("token", "");
        retrofit2.Retrofit retrofit= RetrofitService.getRetrofit();
        UserService userService=retrofit.create(UserService.class);
        Call<User> pp=userService.getUser(userEmail,token);
        pp.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User user = response.body();
                name.setText(user.getFullName());
                email.setText(user.getEmail());
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });


    }
    public void previous(){
        super.onBackPressed();
    }

    public void Initialize(){



    }
}
