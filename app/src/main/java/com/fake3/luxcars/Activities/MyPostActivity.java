package com.fake3.luxcars.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fake3.luxcars.Adapter.MyPostAdapter;
import com.fake3.luxcars.Adapter.SaveListAdapter;
import com.fake3.luxcars.Adapter.VehicleAdapter;
import com.fake3.luxcars.DTO.ReservedDto;
import com.fake3.luxcars.Models.SaveList;
import com.fake3.luxcars.Models.Vehicle;
import com.fake3.luxcars.R;
import com.fake3.luxcars.Services.RetrofitService;
import com.fake3.luxcars.Services.SaveListService;
import com.fake3.luxcars.Services.VehicleService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyPostActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    VehicleAdapter vehicleAdapter;
    List<ReservedDto> myPost;
    ImageView back;
    TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        recyclerView=findViewById(R.id.recycleView);
        back=findViewById(R.id.backicon);
        title=findViewById(R.id.title);
        title.setText("My Posts");
        recyclerView.setHasFixedSize(true);
        SharedPreferences prefs = getSharedPreferences("shared",MODE_PRIVATE);
        String email=  prefs.getString("email", "");
        String token=prefs.getString("token","");
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        myPost=new ArrayList<>();

        retrofit2.Retrofit retrofit= RetrofitService.getRetrofit();
        VehicleService vehicleService=retrofit.create(VehicleService.class);
        Call<List<ReservedDto>> pp=vehicleService.getAllVehicleByUser(email,token);
        pp.enqueue(new Callback<List<ReservedDto>>() {
            @Override
            public void onResponse(Call<List<ReservedDto>> call, Response<List<ReservedDto>> response) {
                myPost = response.body();
                MyPostAdapter postListAdapter=new MyPostAdapter(getApplicationContext(),myPost);
                recyclerView.setAdapter(postListAdapter);
            }
            @Override
            public void onFailure(Call<List<ReservedDto>> call, Throwable t) {

            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                previous();
            }
        });

    }


    public void previous(){
        super.onBackPressed();
    }

}
