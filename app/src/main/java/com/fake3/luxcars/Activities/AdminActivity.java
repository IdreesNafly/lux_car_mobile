package com.fake3.luxcars.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.fake3.luxcars.Admin.AdminActivities.ManageAppointments;
import com.fake3.luxcars.Admin.AdminActivities.ManagePost;
import com.fake3.luxcars.Admin.AdminActivities.ManageReservation;
import com.fake3.luxcars.Admin.AdminActivities.ManageUser;
import com.fake3.luxcars.Admin.AdminActivities.ManageVehicle;
import com.fake3.luxcars.R;

public class AdminActivity extends AppCompatActivity {
CardView manageVehicle,manageUsers,manageAppointment,manageReservation,managePosts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        Toolbar toolbar=findViewById(R.id.toolbars);
        setSupportActionBar(toolbar);

        manageVehicle=findViewById(R.id.manageVehicle);
        manageUsers=findViewById(R.id.manageUser);
        manageAppointment=findViewById(R.id.manageAppointment);
        managePosts=findViewById(R.id.managePosts);
        manageReservation=findViewById(R.id.manageReservation);

        managePosts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(), ManagePost.class);
                startActivity(intent);

            }
        });
        manageVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(), ManageVehicle.class);
                startActivity(intent);

            }
        });
        manageUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(), ManageUser.class);
                startActivity(intent);

            }
        });
        manageAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(), ManageAppointments.class);
                startActivity(intent);

            }
        });
        manageReservation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(), ManageReservation.class);
                startActivity(intent);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.logout, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.logout:
                SharedPreferences preferences =getSharedPreferences("shared", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.apply();
                Intent intent=new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



}
