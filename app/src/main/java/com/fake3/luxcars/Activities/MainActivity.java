package com.fake3.luxcars.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.MenuItem;

import com.fake3.luxcars.Fragments.More;
import com.fake3.luxcars.Fragments.ProfileLogin;
import com.fake3.luxcars.Fragments.Search;
import com.fake3.luxcars.Fragments.Home;
import com.fake3.luxcars.Fragments.Profile;
import com.fake3.luxcars.Fragments.SavedList;
import com.fake3.luxcars.Fragments.Sell;
import com.fake3.luxcars.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView bottomNavigationView=findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(navListner);
        Intent intent=getIntent();
        String command=intent.getStringExtra("command");
        if(command==null){
            command="home";
        }
        switch (command){
            case "profile": getSupportFragmentManager().beginTransaction().replace(R.id.layout,new ProfileLogin()).commit();
            break;
            case "home":getSupportFragmentManager().beginTransaction().replace(R.id.layout,new Home()).commit();
            break;
            default: getSupportFragmentManager().beginTransaction().replace(R.id.layout,new Home()).commit();
            break;


        }


    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListner= new
            BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment slectedFragment=null;
                    SharedPreferences prefs = getSharedPreferences("shared",MODE_PRIVATE);
                    String email=  prefs.getString("email", "");
                    String role=  prefs.getString("admin", "");

                    switch (menuItem.getItemId()){
                        case R.id.nav_home: slectedFragment=new Home();
                            break;
                        case R.id.nav_search: slectedFragment=new Search();
                            break;
                        case R.id.nav_sell: slectedFragment=new Sell();
                            break;
                        case R.id.nav_profile:
                            if(email==null||email==""){
                                slectedFragment=new ProfileLogin();
                            }
                            else {
                                if (role.equals("ROLE_ADMIN")){
                                    Intent intent=new Intent(getApplicationContext(),AdminActivity.class);
                                    startActivity(intent);
                                    break;
                                }
                                else {
                                    slectedFragment = new Profile();
                                }
                            }
                            break;
                        case R.id.nav_settings: slectedFragment=new More();
                            break;
                    }
                    if (role.equals("ROLE_ADMIN")){

                    }
                    else {
                        getSupportFragmentManager().beginTransaction().replace(R.id.layout, slectedFragment).commit();
                    }
                    return true;
                }
            };
}
