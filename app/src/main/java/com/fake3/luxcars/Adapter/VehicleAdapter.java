package com.fake3.luxcars.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.fake3.luxcars.Activities.DetailProduct;
import com.fake3.luxcars.Fragments.DetailFragment;
import com.fake3.luxcars.Models.Vehicle;
import com.fake3.luxcars.R;
import com.fake3.luxcars.Services.RetrofitService;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.PicassoProvider;


import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.crypto.Cipher;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VehicleAdapter extends RecyclerView.Adapter<VehicleAdapter.VehicleViewHolder> {

    private Context context;
    private List<Vehicle> vehicleList;


    public VehicleAdapter(Context context, List<Vehicle> vehicleList) {
        this.context = context;
        this.vehicleList = vehicleList;
    }

    @NonNull
    @Override
    public VehicleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.car_adapter,parent,false);
        VehicleViewHolder holder=new VehicleViewHolder(view);
        return  holder;
}

    @Override
    public void onBindViewHolder(@NonNull VehicleViewHolder holder, int position) {
        final Vehicle vehicle = vehicleList.get(position);
        holder.imageView.setImageResource(R.drawable.ic_person_black_24dp);
        holder.name.setText(vehicle.getVehicleType());
        holder.price.setText("Rs :"+String.valueOf(vehicle.getVehiclePrice()).toString());
        holder.model.setText(vehicle.getVehicleModel());
        holder.year.setText(vehicle.getVehicleYom());

    if(
            vehicle.getCovrimg()==null
    ){
        Picasso.get().load("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTs4gAhdje5HbqJ9yVq9O1LSnf7ui6Dh1iG9SMX3u6ZLFSAc4caxw&s").into(holder.imageView);
    }
    else{
        String image=vehicle.getCovrimg();
        int index = image.lastIndexOf('/');
        String lastString = image.substring(index +1);
        String updatedImage= RetrofitService.url()+"download/"+lastString;

        Picasso.get()
                .load(Uri.parse(updatedImage))
                .fit()
                .centerCrop()
                .into(holder.imageView);

        /*
        Bitmap bitmap = null;

        try {

            URL urlImage = new URL(
                    vehicle.getCovrimg());
            HttpURLConnection connection = (HttpURLConnection) urlImage
                    .openConnection();
            InputStream inputStream = connection.getInputStream();
            bitmap = BitmapFactory.decodeStream(inputStream);
            holder.imageView.setImageBitmap(bitmap);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
*/





      //  Picasso.get().load(vehicle.getVehicleImages().get(1).getImagePath()).into(holder.imageView);

    }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.layout,new DetailFragment()).commit();
                Intent intent=new Intent( context, DetailProduct.class);
                intent.putExtra("id",vehicle.getId());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }



    @Override
    public int getItemCount() {
         return vehicleList.size();
    }

    class VehicleViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView name,price,year,model;
        CardView cardView;

        public VehicleViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView=itemView.findViewById(R.id.cardView);
            imageView =itemView.findViewById(R.id.imageView);
            name=itemView.findViewById(R.id.textViewTitle);
            model=itemView.findViewById(R.id.textViewModel);
            year=itemView.findViewById(R.id.textViewyear);
            price=itemView.findViewById(R.id.textViewPrice);


        }
    }

}
