package com.fake3.luxcars.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.fake3.luxcars.Activities.DetailProduct;
import com.fake3.luxcars.DTO.ReservedDto;
import com.fake3.luxcars.DTO.VehicleDTO;
import com.fake3.luxcars.Models.SaveList;
import com.fake3.luxcars.Models.Vehicle;
import com.fake3.luxcars.R;
import com.fake3.luxcars.Services.RetrofitService;
import com.fake3.luxcars.Services.VehicleService;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MyPostAdapter extends RecyclerView.Adapter<MyPostAdapter.VehicleViewHolder> {

    private Context context;
    private List<ReservedDto> myPost;


    public MyPostAdapter(Context context, List<ReservedDto> myPost) {
        this.context = context;
        this.myPost = myPost;
    }
    @NonNull
    @Override
    public MyPostAdapter.VehicleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.adapter_my_post,parent,false);
        MyPostAdapter.VehicleViewHolder holder=new MyPostAdapter.VehicleViewHolder(view);
        return  holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyPostAdapter.VehicleViewHolder holder, final int position) {
        final ReservedDto vehicle = myPost.get(position);
        SharedPreferences prefs =  context.getSharedPreferences("shared",Context.MODE_PRIVATE);
        String email=  prefs.getString("email", "");
        final String token=prefs.getString("token","");
        holder.fuel.setText(vehicle.getVehicleFuel());
        holder.km.setText(vehicle.getVehicleMileage());
        holder.colour.setText(vehicle.getVehicleExteriorColor());
        holder.type.setText(vehicle.getVehicleType());
        holder.auto.setText(vehicle.getVehicleTransmission());
        holder.price.setText(String.valueOf(vehicle.getVehiclePrice()));
        holder.status.setText(vehicle.getStatus());
        if(vehicle.getStatus().equals("Sold")){
            holder.update.setVisibility(View.GONE);
        }
        holder.update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alertbox = new AlertDialog.Builder(view.getRootView().getContext());
                alertbox.setMessage("Are You Sure You Want to Make This Vehicle as Sold");
                alertbox.setTitle("Conformation");
                alertbox.setIcon(android.R.drawable.ic_dialog_alert);

                alertbox.setNeutralButton("OK",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface arg0,
                                                int arg1) {
                                Retrofit retrofit = RetrofitService.getRetrofit();
                                VehicleService vehicleService = retrofit.create(VehicleService.class);
                                VehicleDTO vehicleDTO = new VehicleDTO(vehicle.getId(), "Sold");
                                Call<Object> pp = vehicleService.updateVehicle(vehicleDTO, token);
                                pp.enqueue(new Callback<Object>() {
                                    @Override
                                    public void onResponse(Call<Object> call, Response<Object> response) {
                                        Object j = response.body();
                                        if (j.equals(true) ) {
                                            notifyItemChanged(position);
                                            Toast.makeText(context, "SucessFully Updated Status", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                    @Override
                                    public void onFailure(Call<Object> call, Throwable thtow) {
                                    }
                                });
                            }
                        });
                alertbox.show();
//                new AlertDialog.Builder(context)
//                        .setTitle("Conformation")
//                        .setMessage("Are You Sure You Want to Make This Vehicle as Sold")
//                        .setIcon(android.R.drawable.ic_dialog_alert)
//                        .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
//
//                            public void onClick(DialogInterface dialog, int whichButton) {
//                                Retrofit retrofit = RetrofitService.getRetrofit();
//                                VehicleService vehicleService = retrofit.create(VehicleService.class);
//                                VehicleDTO vehicleDTO = new VehicleDTO(vehicle.getId(), "Sold");
//                                Call<Object> pp = vehicleService.updateVehicle(vehicleDTO, token);
//                                pp.enqueue(new Callback<Object>() {
//                                    @Override
//                                    public void onResponse(Call<Object> call, Response<Object> response) {
//                                        Object j = response.body();
//                                        if (j.equals(true) ) {
//                                            notifyItemChanged(position);
//                                            Toast.makeText(context, "SucessFully Updated Status", Toast.LENGTH_SHORT).show();
//                                        }
//                                    }
//                                    @Override
//                                    public void onFailure(Call<Object> call, Throwable thtow) {
//                                    }
//                                });
//                            }
//                        })
//                        .setNegativeButton("Cancel", null).show();
            }
        });
        holder.make.setText(vehicle.getVehicleMake()+" "+
                vehicle.getVehicleModel()+" "+
                vehicle.getVehicleYom());

        if(
                vehicle.getCovrimg()==null
        ){
            Picasso.get().load("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTs4gAhdje5HbqJ9yVq9O1LSnf7ui6Dh1iG9SMX3u6ZLFSAc4caxw&s").into(holder.imageView);
        }
        else {
            String image=vehicle.getCovrimg();
            int index = image.lastIndexOf('/');
            String lastString = image.substring(index +1);

            String updatedImage= RetrofitService.url()+"download/"+lastString;

            Picasso.get()
                    .load(Uri.parse(updatedImage))
                    .fit()
                    .centerCrop()
                    .into(holder.imageView);
        }

        holder.viewPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent((context), DetailProduct.class);
                intent.putExtra("id",vehicle.getId());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return myPost.size();
    }

    class VehicleViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView km,type,colour,fuel,auto,make,price,status;
        CardView viewPost;
        Button update;

        public VehicleViewHolder(@NonNull View itemView) {
            super(itemView);
            viewPost=itemView.findViewById(R.id.viewPost);
            imageView =itemView.findViewById(R.id.image);
            km=itemView.findViewById(R.id.km);
            make=itemView.findViewById(R.id.make);
            type=itemView.findViewById(R.id.type);
            auto=itemView.findViewById(R.id.Auto);
            fuel=itemView.findViewById(R.id.fuel);
            price=itemView.findViewById(R.id.price);
            update=itemView.findViewById(R.id.update);
            status=itemView.findViewById(R.id.status);
            colour=itemView.findViewById(R.id.interiorColour);



        }
    }

}


