package com.fake3.luxcars.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.models.SlideModel;
import com.fake3.luxcars.Activities.ActivityAppointment;
import com.fake3.luxcars.Activities.DetailProduct;
import com.fake3.luxcars.Activities.SaveListActivity;
import com.fake3.luxcars.Fragments.DetailFragment;
import com.fake3.luxcars.Models.SaveList;
import com.fake3.luxcars.Models.Vehicle;
import com.fake3.luxcars.Models.VehicleImages;
import com.fake3.luxcars.R;
import com.fake3.luxcars.Services.AppointmentService;
import com.fake3.luxcars.Services.RetrofitService;
import com.fake3.luxcars.Services.SaveListService;
import com.fake3.luxcars.Services.VehicleService;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.PicassoProvider;


import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.Cipher;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SaveListAdapter extends RecyclerView.Adapter<SaveListAdapter.VehicleViewHolder> {

    private Context context;
    private List<SaveList> saveList;


    public SaveListAdapter(Context context, List<SaveList> saveList) {
        this.context = context;
        this.saveList = saveList;
    }

    @NonNull
    @Override
    public VehicleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.save_list_adapter,null);
        VehicleViewHolder holder=new VehicleViewHolder(view);
        return  holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final VehicleViewHolder holder, final int position) {
        final SaveList savelist = saveList.get(position);
//        holder.imageView.setImageResource(R.drawable.ic_person_black_24dp);
        holder.model.setText(savelist.getVehicleid().getVehicleMake()+" "+savelist.getVehicleid().getVehicleModel());
        holder.year.setText(savelist.getVehicleid().getVehicleYom());
        holder.price.setText(String.valueOf(savelist.getVehicleid().getVehiclePrice()));

        if(
                savelist.getVehicleid().getCovrimg()==null
        ){
            Picasso.get().load("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTs4gAhdje5HbqJ9yVq9O1LSnf7ui6Dh1iG9SMX3u6ZLFSAc4caxw&s").into(holder.imageView);
        }
        else {
            String image=savelist.getVehicleid().getCovrimg();
            int index = image.lastIndexOf('/');
            String lastString = image.substring(index +1);
            String updatedImage= RetrofitService.url()+"download/"+lastString;

            Picasso.get()
                    .load(Uri.parse(updatedImage))
                    .fit()
                    .centerCrop()
                    .into(holder.imageView);
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent((context), DetailProduct.class);
                intent.putExtra("id",savelist.getVehicleid().getId());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences prefs =  context.getSharedPreferences("shared",Context.MODE_PRIVATE);
                String email=  prefs.getString("email", "");
                String token=prefs.getString("token","");
                retrofit2.Retrofit retrofit = RetrofitService.getRetrofit();
                SaveListService saveListService = retrofit.create(SaveListService.class);
                Call<ResponseBody> pp = saveListService.deleteItem(savelist.getId(),token);
                pp.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            String Response = response.body().string();
                            if(Response.equals("Removed From Saved List")){
//                                Intent intent=new Intent(context, SaveListActivity.class);
//                                 context.startActivity(intent);
                                saveList.remove(position);
                              //  context.removeViewAt(position);
                                notifyItemRemoved(position);
                                notifyItemRangeChanged(position, saveList.size());
                                notifyDataSetChanged();
                            }
                            else {
                                Toast.makeText(context, "Erro Removing", Toast.LENGTH_SHORT).show();
                            }
                        }catch (Exception e){
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable thtow) {
                    }
                });
            }
        });

    }



    @Override
    public int getItemCount() {
        return saveList.size();
    }

    class VehicleViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView model,year,price;
        CardView cardView;
        Button delete;

        public VehicleViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView=itemView.findViewById(R.id.cardview);
            imageView =itemView.findViewById(R.id.image);
            model=itemView.findViewById(R.id.model);
            year=itemView.findViewById(R.id.year);
            price=itemView.findViewById(R.id.price);
            delete=itemView.findViewById(R.id.delete);

        }
    }

}
