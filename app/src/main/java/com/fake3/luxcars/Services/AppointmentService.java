package com.fake3.luxcars.Services;

import com.fake3.luxcars.DTO.AppointmentDTO;
import com.fake3.luxcars.DTO.AppointmentRequest;
import com.fake3.luxcars.DTO.SaveListDTO;
import com.fake3.luxcars.DTO.VehicleDTO;
import com.fake3.luxcars.Models.Appointments;
import com.fake3.luxcars.Models.Vehicle;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface AppointmentService {

    @POST("api/appointments/makeAppointments")
    Call<Object> makeAppointments(@Body AppointmentDTO appointmentDTO, @Header("Authorization") String accessToken);

    @GET("api/appointments/getAppointmentByUser/{email}")
    Call<List<Appointments>> getAppointmentByUser(@Path("email") String email, @Header("Authorization") String accessToken);

    @PUT("api/appointments/cancelAppointment/{id}")
    Call<ResponseBody> cancelAppointment(@Path("id") Long id, @Header("Authorization") String accessToken);

    @GET("api/appointments/getAllAppointment")
    Call<List<Appointments>> getAllAppointment(@Header("Authorization") String accessToken);

    @PUT("api/appointments/respondToAppointment")
    Call<ResponseBody> respondToAppointment(@Body AppointmentRequest appointmentRequest, @Header("Authorization") String accessToken);


}
