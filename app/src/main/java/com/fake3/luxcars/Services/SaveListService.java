package com.fake3.luxcars.Services;

import com.fake3.luxcars.Models.SaveList;
import com.fake3.luxcars.DTO.SaveListDTO;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface SaveListService {

    @POST("api/savedlist/addtosavedlist")
    Call<ResponseBody> addSaveList(@Body SaveListDTO savedList, @Header("Authorization") String accessToken);

    @GET("api/savedlist/get/{id}")
    Call<List<SaveList>> getSaveList(@Path("id") String id, @Header("Authorization") String accessToken);

    @DELETE("api/savedlist/deleteItem/{id}")
    Call<ResponseBody> deleteItem(@Path("id") Long id, @Header("Authorization") String accessToken);

}
