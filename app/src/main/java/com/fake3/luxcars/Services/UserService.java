package com.fake3.luxcars.Services;

import com.fake3.luxcars.DTO.UserDTO;
import com.fake3.luxcars.Models.JwtResponse;
import com.fake3.luxcars.Models.SignInRequest;
import com.fake3.luxcars.Models.User;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface UserService {


    @POST("api/auth/signup")
    Call<User> registerUser(@Body User user);

    @POST("api/auth/signin")
    Call<JwtResponse> loginUser(@Body SignInRequest user);

    @GET("api/users/getUser/{id}")
    Call<User> getUser(@Path("id") String id, @Header("Authorization") String accessToken);

    @GET("api/users/getAllUser")
    Call<List<User>> getAllUser(@Header("Authorization") String accessToken);

    @DELETE("api/users/deleteUser/{id}")
    Call<ResponseBody> deleteUser(@Path("id") String id, @Header("Authorization") String accessToken);

}
