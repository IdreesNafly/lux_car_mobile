package com.fake3.luxcars.Services;

import com.fake3.luxcars.Models.Email;
import com.fake3.luxcars.Models.JwtResponse;
import com.fake3.luxcars.Models.SignInRequest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface EmailService {

    @POST("mailservice/mailTo")
    Call<Object> mailTo(@Body Email email);
}
