package com.fake3.luxcars.Services;

import com.fake3.luxcars.DTO.ReservedDto;
import com.fake3.luxcars.DTO.ReservedVehicles;
import com.fake3.luxcars.DTO.VehicleDTO;
import com.fake3.luxcars.DTO.VehicleRequest;
import com.fake3.luxcars.Models.Vehicle;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface VehicleService {

    @GET("api/vehicle/getResVehicle")
    Call<List<Vehicle>> getVehicles();

    @GET("api/vehicle/getResVehicleById/{id}")
    Call<Vehicle> getVehicleById(@Path("id") Long id);

    @PUT("api/vehicle/updateVehicleStatus")
    Call<Object> updateVehicle(@Body VehicleDTO vehicleDTO, @Header("Authorization") String accessToken);

    @PUT("api/vehicle/cancelReservation")
    Call<Object> cancelReservation(@Body VehicleDTO vehicleDTO, @Header("Authorization") String accessToken);

    @PUT("api/vehicle/approveAd/{id}")
    Call<Object> approvedAd(@Path("id") Long id, @Header("Authorization") String accessToken);

    @PUT("api/vehicle/updateVehiclePrice")
    Call<Object> updateVehiclePrice(@Body VehicleRequest vehicleRequest, @Header("Authorization") String accessToken);

    @DELETE("api/vehicle/deleteVehicle/{id}")
    Call<Object> deleteVehicle(@Path("id") Long id, @Header("Authorization") String accessToken);

    @GET("api/vehicle/search/{make}/{model}/{year}")
    Call<List<Vehicle>> search(@Path("make") String make,@Path("model") String model,@Path("year") String year);

    @GET("api/vehicle/searchByType/{type}")
    Call<List<Vehicle>> searchByType(@Path("type") String type);

    @GET("api/vehicle/getAllVehicleByUser/{email}")
    Call<List<ReservedDto>> getAllVehicleByUser(@Path("email") String email, @Header("Authorization") String accessToken);

    @GET("api/vehicle/getMobReservedVehicles/{email}")
    Call<List<ReservedVehicles>> getReservedVehicles(@Path("email") String email, @Header("Authorization") String accessToken);

    @GET("api/vehicle/getMobAllReservedVehicles")
    Call<List<ReservedVehicles>> getAllReservedVehicles(@Header("Authorization") String accessToken);

    @Multipart
    @POST("api/vehicle/postAd")
    Call<Object> postAd(@Query("files") MultipartBody.Part[] files, @Query("vehicle") String vehicle, @Header("Authorization") String accessToken);
}
