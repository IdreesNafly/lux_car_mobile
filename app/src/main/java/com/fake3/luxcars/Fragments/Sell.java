package com.fake3.luxcars.Fragments;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.FileUtils;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.Toolbar;

import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.models.SlideModel;
import com.fake3.luxcars.Activities.MainActivity;
import com.fake3.luxcars.Models.User;
import com.fake3.luxcars.Models.Vehicle;
import com.fake3.luxcars.R;
import com.fake3.luxcars.Services.RetrofitService;
import com.fake3.luxcars.Services.UserService;
import com.fake3.luxcars.Services.VehicleService;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Multipart;

public class Sell extends Fragment {
    EditText makes,model,year,millage,price,interior,exterior,message;
    String _make,_model,_year,_millage,_price,_type,_transmission,_interior,_exterior,_fuel,_message;
    Button sell,photo;
    private String[] cars = {"SUV","Sedan","Couple Van"};
    private String[] types = {"SUV","Sedan","Couple Van"};
    private String[] fuels = {"Hybrid","Petrol","Diesel"};
    private String[] transmissions = {"Automatic","Manual"};
    private Spinner spCars,type,fuel,transmission;
    private static final int RESULT_OK = -1;




    private static int RESULT_LOAD_IMAGE = 1;
    public Sell() {
        // Required empty public constructor
        }


@Override
public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_sell, container, false);
    initialize(view);
    spCars = view.findViewById(R.id.spCars);

    fuel.setAdapter(new ArrayAdapter(getContext(),android.R.layout.simple_spinner_dropdown_item,fuels));
    spCars.setAdapter(new ArrayAdapter(getContext(),android.R.layout.simple_spinner_dropdown_item,cars));
    type.setAdapter(new ArrayAdapter(getContext(),android.R.layout.simple_spinner_dropdown_item,types));
    transmission.setAdapter(new ArrayAdapter(getContext(),android.R.layout.simple_spinner_dropdown_item,transmissions));

    photo.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setType("image/*"); //allows any image file type. Change * to specific extension to limit it
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1); //SELECT_PICTURES is simply a global int used to check the calling intent in onActivityResult
        }
    });


    sell.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            get();
            SharedPreferences prefs = getActivity().getSharedPreferences("shared", Context.MODE_PRIVATE);
            String email=  prefs.getString("email", "");
            String token=prefs.getString("token","");
            Vehicle vehicle=new Vehicle(_make,_model,_type,_exterior,_interior,_year,_message,_fuel,_transmission,_millage,Double.parseDouble(_price),email);
            Gson gson = new Gson();
            String json = gson.toJson(vehicle);
            retrofit2.Retrofit retrofit = RetrofitService.getRetrofit();
            VehicleService vehicleService = retrofit.create(VehicleService.class);

//            MultipartBody.Builder builder = new MultipartBody.Builder();
//            builder.setType(MultipartBody.FORM);
//
//            builder.addFormDataPart("user_name", "Robert");
//            builder.addFormDataPart("email", "mobile.apps.pro.vn@gmail.com");
//
//            // Map is used to multipart the file using okhttp3.RequestBody
//            // Multiple Images
//            for (int i = 0; i < arrayList.size(); i++) {
//                File file = FileUtils.getFile(this, fileUri);
//                File file = new File(filePaths.get(i));
//                builder.addFormDataPart("file[]", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));
//            }
//
//
//            MultipartBody requestBody = builder.build();
            RequestBody[] image_id = new RequestBody[arrayList.size()];
            for(int i=0;i<arrayList.size();i++){
                File file = new File(arrayList.get(i).toString());
                RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
                image_id[i]=requestBody;
            }

            MultipartBody.Part[] array = new MultipartBody.Part[arrayList.size()];
            for(int i=0;i<arrayList.size();i++){
                File file = new File(arrayList.get(i).toString());
                RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part filePart = MultipartBody.Part.createFormData("value_"
                                , file.getName(),
                        requestBody);
                array[i]=filePart;
            }
            Call<Object> pp = vehicleService.postAd(array ,json,token);
            pp.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    Object r = response.body();
                    if (r != null) {
                     //   Toast.makeText(getApplicationContext(), "User successfully created", Toast.LENGTH_LONG).show();
                       // previous();
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable thtow) {
                }
            });
        }
    });


    return view;
        }



    int PICK_IMAGE_MULTIPLE = 1;
    String imageEncoded;
    ArrayList<Uri> arrayList = new ArrayList<>();
    List<String> imagesEncodedList;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        super.onActivityResult(requestCode, resultCode, resultData);
//       if(requestCode == 1) {
//            if(resultCode == 1) {
//                final List<Bitmap> bitmaps=new ArrayList<>();
//                ClipData clipData=data.getClipData();
//                if(clipData != null) {
//                    int count = clipData.getItemCount(); //evaluate the count before the for loop --- otherwise, the count is evaluated every loop.
//                    for(int i = 0; i < count; i++) {
//                        Uri imageUri = data.getClipData().getItemAt(i).getUri();
//                        try {
//                            InputStream is=getActivity().getContentResolver().openInputStream(imageUri);
//                            Bitmap bitmap= BitmapFactory.decodeStream(is);
//                            bitmaps.add(bitmap);
//                        } catch (FileNotFoundException e) {
//                            e.printStackTrace();
//                        }
//                        //do something with the image (save it to some directory or whatever you need to do with it here)
//                }
//            } else  {
//                    Uri imageUri=data.getData();
//                    try {
//                        InputStream is=getActivity().getContentResolver().openInputStream(imageUri);
//                        Bitmap bitmap= BitmapFactory.decodeStream(is);
//                        bitmaps.add(bitmap);
//                    } catch (FileNotFoundException e) {
//                        e.printStackTrace();
//                    }
//                    //  String imagePath = data.getData().getPath();
//                //do something with the image (save it to some directory or whatever you need to do with it here)
//            }
//                new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        for(final Bitmap b:bitmaps){
//                 getActivity().runOnUiThread(new Runnable() {
//                     @Override
//                     public void run() {
//                        image.setImageBitmap(b);
//                         }
//                            });
//                            try {
//                                Thread.sleep(3000);
//                            } catch (InterruptedException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    }
//                }).start();
//        }
//    }
        if (resultCode == RESULT_OK) {

            if (requestCode == 1) {
                if (resultData != null) {

                    if (resultData.getClipData() != null) {
                        int count = resultData.getClipData().getItemCount();
                        int currentItem = 0;

                        while (currentItem < count) {
                            Uri imageUri = resultData.getClipData().getItemAt(currentItem).getUri();
                            currentItem = currentItem + 1;

                            Log.d("Uri Selected", imageUri.toString());

                            arrayList.add(imageUri);
//                                arrayList.add(imageUri);
//                                MyAdapter mAdapter = new MyAdapter(MainActivity.this, arrayList);
//                                listView.setAdapter(mAdapter);mAdapter
                        }
                    }
                    else if (resultData.getData() != null) {
                        final Uri uri = resultData.getData();
                            arrayList=new ArrayList();
                            arrayList.add(uri);
                    }
                }
            }
        }

}

        public void get(){
        if(makes.getText().toString().isEmpty()){
            makes.setError("Enter Vehicle Make");
        }
        else if(model.getText().toString().isEmpty()||year.getText().toString().isEmpty()||millage.getText().toString().isEmpty()||
                price.getText().toString().isEmpty()||interior.getText().toString().isEmpty()||exterior.getText().toString().isEmpty()||
                message.getText().toString().isEmpty()||
                transmission.getSelectedItem().toString().isEmpty()||
                fuel.getSelectedItem().toString().isEmpty()||
                type.getSelectedItem().toString().isEmpty()){
            Toast.makeText(getContext(), "Please Enter All fields ", Toast.LENGTH_SHORT).show();
        }
        else {
            _make = makes.getText().toString();
            _model = model.getText().toString();
            _year = year.getText().toString();
            _millage = millage.getText().toString();
            _price = price.getText().toString();
            _type = type.getSelectedItem().toString();
            _transmission = transmission.getSelectedItem().toString();
            _interior = interior.getText().toString();
            _exterior = exterior.getText().toString();
            _fuel = fuel.getSelectedItem().toString();
            _message = message.getText().toString();
        }
        }

       public void initialize(View view){
        sell=view.findViewById(R.id.sell);
        makes=view.findViewById(R.id.make);
        model=view.findViewById(R.id.model);
        year=view.findViewById(R.id.year);
        millage=view.findViewById(R.id.millage);
        price=view.findViewById(R.id.price);
        photo=view.findViewById(R.id.photo);
        type = view.findViewById(R.id.type);
        transmission = view.findViewById(R.id.transmission);
        fuel=view.findViewById(R.id.fuel);
        interior=view.findViewById(R.id.interior);
        exterior=view.findViewById(R.id.exterior);
        message=view.findViewById(R.id.message);

       }


}