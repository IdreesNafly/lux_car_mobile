package com.fake3.luxcars.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fake3.luxcars.Adapter.VehicleAdapter;
import com.fake3.luxcars.Models.Vehicle;
import com.fake3.luxcars.R;

import java.util.ArrayList;
import java.util.List;

public class SearchResult extends Fragment {
    RecyclerView recyclerView;
    VehicleAdapter vehicleAdapter;
    List<Vehicle> vehicleList;
    ImageView back;
    TextView title;
    public SearchResult() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_search_result, container, false);

        back=view.findViewById(R.id.backicon);
        title=view.findViewById(R.id.title);
        title.setText("Search Results");

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                previous();
            }
        });


        recyclerView=view.findViewById(R.id.recycleView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        vehicleList=new ArrayList<>();
        List<Vehicle> obj= (List<Vehicle>) getArguments().getSerializable("KEY_ARRAYLIST");
        vehicleAdapter=new VehicleAdapter(getContext(),obj);
        recyclerView.setAdapter(vehicleAdapter);

        return  view;
    }

    public void previous(){
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.layout,new Search()).commit();
    }


    }
