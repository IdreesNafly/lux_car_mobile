package com.fake3.luxcars.Fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.fake3.luxcars.Adapter.VehicleAdapter;
import com.fake3.luxcars.Models.User;
import com.fake3.luxcars.Models.Vehicle;
import com.fake3.luxcars.R;
import com.fake3.luxcars.Services.RetrofitService;
import com.fake3.luxcars.Services.UserService;
import com.fake3.luxcars.Services.VehicleService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProductHome extends Fragment {

    RecyclerView recyclerView;
    VehicleAdapter vehicleAdapter;
    List<Vehicle> vehicleList;
    public ProductHome() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_product_home, container, false);
        recyclerView=view.findViewById(R.id.recycleView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        vehicleList=new ArrayList<>();
        retrofit2.Retrofit retrofit= RetrofitService.getRetrofit();
        VehicleService vehicleService=retrofit.create(VehicleService.class);
        Call<List<Vehicle>> pp=vehicleService.getVehicles();
        pp.enqueue(new Callback<List<Vehicle>>() {
            @Override
            public void onResponse(Call<List<Vehicle>> call, Response<List<Vehicle>> response) {
                vehicleList = response.body();
                List<Vehicle> newPost=new ArrayList<>();
                for (Vehicle vehicle:vehicleList
                ) {
                    if(vehicle.getStatus().equals("Available")){
                        newPost.add(vehicle);
                    }
                }
                vehicleAdapter=new VehicleAdapter(getContext(),newPost);
                recyclerView.setAdapter(vehicleAdapter);
            }

            @Override
            public void onFailure(Call<List<Vehicle>> call, Throwable t) {
                if (t instanceof IOException) {
                    Toast.makeText(getContext(), "this is an actual network failure :( inform the user and possibly retry", Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                }
                else {
                    Toast.makeText(getContext(), "conversion issue! big problems :(", Toast.LENGTH_SHORT).show();
                    // todo log to some central bug tracking service
                }
            }
        });







        return  view;
    }

}
