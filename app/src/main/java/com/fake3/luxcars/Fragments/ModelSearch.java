package com.fake3.luxcars.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.fake3.luxcars.Models.Vehicle;
import com.fake3.luxcars.R;
import com.fake3.luxcars.Services.RetrofitService;
import com.fake3.luxcars.Services.SaveListService;
import com.fake3.luxcars.Services.VehicleService;

import java.io.Serializable;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ModelSearch extends Fragment {

Button suv,van,couple,sedan,search;
String make,model,year;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_model_search, container, false);
        suv=view.findViewById(R.id.suv1);
        van=view.findViewById(R.id.van1);
        couple=view.findViewById(R.id.couple);
        sedan=view.findViewById(R.id.sedan);
        search=view.findViewById(R.id.search);



        suv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            searchByType("SUV");
            }
        });
        van.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchByType("VAN");
            }
        });

        couple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchByType("COUPE");
            }
        });

        sedan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchByType("SEDAN");
            }
        });

        final EditText dropdown =view.findViewById(R.id.spinner);
        final EditText dropdown1 =view.findViewById(R.id.type);
        final EditText dropdown2 =view.findViewById(R.id.transmission);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                make=dropdown.getText().toString();
                model=dropdown1.getText().toString();
                year=dropdown2.getText().toString();
                if (make.isEmpty()) {
                    dropdown.setError("Enter Vehicle Make");
                } else if (model.isEmpty()) {
                    dropdown1.setError("Enter Vehicle  Model");
                } else if (year.isEmpty()) {
                    dropdown2.setError("Enter Vehicle Year");
                } else {
                    Retrofit retrofit = RetrofitService.getRetrofit();
                    VehicleService vehicleService = retrofit.create(VehicleService.class);
                    Call<List<Vehicle>> pp = vehicleService.search(make, model, year);
                    pp.enqueue(new Callback<List<Vehicle>>() {
                        @Override
                        public void onResponse(Call<List<Vehicle>> call, Response<List<Vehicle>> response) {
                            try {
                                List<Vehicle> aa = response.body();
                                if(aa.size()!=0) {
                                Bundle bundle = new Bundle();
                               // bundle.putString("latitude", latitude);
                                bundle.putSerializable("KEY_ARRAYLIST", (Serializable) aa);

                                SearchResult mapFragment = new SearchResult();
                                mapFragment.setArguments(bundle);

                                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.layout, mapFragment).commit();
                                }
                                else{
                                    Toast.makeText(getContext(), "No Vehicles Available" , Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {

                            }
                        }

                        @Override
                        public void onFailure(Call<List<Vehicle>> call, Throwable thtow) {
                        }
                    });
                }
            }
        });







        return view;
    }

    public void catagory(){
        

    }

    public void searchByType(final String type){
        Retrofit retrofit = RetrofitService.getRetrofit();
        VehicleService vehicleService = retrofit.create(VehicleService.class);
        Call<List<Vehicle>> pp = vehicleService.searchByType(type);
        pp.enqueue(new Callback<List<Vehicle>>() {
            @Override
            public void onResponse(Call<List<Vehicle>> call, Response<List<Vehicle>> response) {
                try {
                    List<Vehicle> aa = response.body();
                    if(aa.size()!=0) {
                        Bundle bundle = new Bundle();
                        // bundle.putString("latitude", latitude);
                        bundle.putSerializable("KEY_ARRAYLIST", (Serializable) aa);
                        SearchResult mapFragment = new SearchResult();
                        mapFragment.setArguments(bundle);
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.layout, mapFragment).commit();
                    }
                    else{
                        Toast.makeText(getContext(), "No "+type+" vehicles Available" , Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Call<List<Vehicle>> call, Throwable thtow) {
            }
        });

    }

}