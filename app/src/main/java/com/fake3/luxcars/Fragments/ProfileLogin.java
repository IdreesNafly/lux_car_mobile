package com.fake3.luxcars.Fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fake3.luxcars.Activities.Login;
import com.fake3.luxcars.Activities.Register;
import com.fake3.luxcars.R;

public class ProfileLogin extends Fragment {


    public ProfileLogin() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_profile_login, container, false);
        TextView signin=view.findViewById(R.id.logbtn);
        TextView signup=view.findViewById(R.id.regbtn);

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(), Login.class);
                intent.putExtra("sample","login");
                startActivity(intent);
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(), Register.class);
                startActivity(intent);
            }
        });
        return view;
    }
    }
