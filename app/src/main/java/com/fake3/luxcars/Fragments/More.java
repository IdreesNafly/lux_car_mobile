package com.fake3.luxcars.Fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fake3.luxcars.Admin.AdminAdapters.TabAdapter;
import com.fake3.luxcars.Admin.AdminFragments.ManagePost.MyPost;
import com.fake3.luxcars.Admin.AdminFragments.ManagePost.PendingPost;
import com.fake3.luxcars.R;
import com.google.android.material.tabs.TabLayout;


public class More extends Fragment {
    private TabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    public More() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_more, container, false);
        viewPager = view.findViewById(R.id.viewPager);
        tabLayout = view.findViewById(R.id.tabLayout);
        adapter = new TabAdapter(getActivity().getSupportFragmentManager());
        adapter.addFragment(new Reviews(), "Reviews");
        adapter.addFragment(new ContactUs(), "Contact Us");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        return  view;
    }

}
