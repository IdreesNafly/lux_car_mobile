package com.fake3.luxcars.DTO;

public class VehicleRequest {

    private Long vehicleId;

    private  double vehiclePrice;

    public VehicleRequest(Long vehicleId, double price) {
        this.vehicleId = vehicleId;
        this.vehiclePrice = price;
    }

    public Long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public double getPrice() {
        return vehiclePrice;
    }

    public void setPrice(double price) {
        this.vehiclePrice = price;
    }
}
