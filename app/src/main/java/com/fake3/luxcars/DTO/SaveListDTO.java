package com.fake3.luxcars.DTO;

public class SaveListDTO {

    private long vehicleid;

    private String userid;

    public SaveListDTO(long vehicleid, String userid) {
        this.vehicleid = vehicleid;
        this.userid = userid;
    }
}
