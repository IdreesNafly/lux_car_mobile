package com.fake3.luxcars.DTO;

import com.fake3.luxcars.Models.Appointments;

import java.sql.Date;
import java.util.List;

public class ReservedDto {

    private long id;
    private String vehicleMake;
    private String vehicleModel;
    private String vehicleType;
    private String vehicleExteriorColor;
    private String vehicleInteriorColor;
    private String vehicleYom;
    private String vehicleDescription;
    private String vehicleFuel;
    private String vehicleTransmission;
    private String vehicleMileage;
    private Date reservedDate;
    private double vehiclePrice;
    private String status;
    private String reservedBy;
    private Boolean isAd;
    private Boolean isAdApproved;
    private String images;
    private String userid;
    private String covrimg;
    List<Appointments> appointments;
    List<String> imageList;

    public ReservedDto(long id, String vehicleMake, String vehicleModel, String vehicleType, String vehicleExteriorColor, String vehicleInteriorColor, String vehicleYom, String vehicleDescription, String vehicleFuel, String vehicleTransmission, String vehicleMileage, Date reservedDate, double vehiclePrice, String status, String reservedBy, Boolean isAd, Boolean isAdApproved, String images, String userid, String covrimg, List<Appointments> appointments, List<String> imageList) {
        this.id = id;
        this.vehicleMake = vehicleMake;
        this.vehicleModel = vehicleModel;
        this.vehicleType = vehicleType;
        this.vehicleExteriorColor = vehicleExteriorColor;
        this.vehicleInteriorColor = vehicleInteriorColor;
        this.vehicleYom = vehicleYom;
        this.vehicleDescription = vehicleDescription;
        this.vehicleFuel = vehicleFuel;
        this.vehicleTransmission = vehicleTransmission;
        this.vehicleMileage = vehicleMileage;
        this.reservedDate = reservedDate;
        this.vehiclePrice = vehiclePrice;
        this.status = status;
        this.reservedBy = reservedBy;
        this.isAd = isAd;
        this.isAdApproved = isAdApproved;
        this.images = images;
        this.userid = userid;
        this.covrimg = covrimg;
        this.appointments = appointments;
        this.imageList = imageList;
    }

    public ReservedDto(long id, String vehicleMake, String vehicleModel, String vehicleType, String vehicleExteriorColor, String vehicleInteriorColor, String vehicleYom, String vehicleDescription, String vehicleFuel, String vehicleTransmission, String vehicleMileage, double vehiclePrice, String status, String reservedBy, Boolean isAd, Boolean isAdApproved, String images, String userid, String covrimg, List<Appointments> appointments, List<String> imageList) {
        this.id = id;
        this.vehicleMake = vehicleMake;
        this.vehicleModel = vehicleModel;
        this.vehicleType = vehicleType;
        this.vehicleExteriorColor = vehicleExteriorColor;
        this.vehicleInteriorColor = vehicleInteriorColor;
        this.vehicleYom = vehicleYom;
        this.vehicleDescription = vehicleDescription;
        this.vehicleFuel = vehicleFuel;
        this.vehicleTransmission = vehicleTransmission;
        this.vehicleMileage = vehicleMileage;
        this.vehiclePrice = vehiclePrice;
        this.status = status;
        this.reservedBy = reservedBy;
        this.isAd = isAd;
        this.isAdApproved = isAdApproved;
        this.images = images;
        this.userid = userid;
        this.covrimg = covrimg;
        this.appointments = appointments;
        this.imageList = imageList;
    }

    public Date getReservedDate() {
        return reservedDate;
    }

    public void setReservedDate(Date reservedDate) {
        this.reservedDate = reservedDate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleExteriorColor() {
        return vehicleExteriorColor;
    }

    public void setVehicleExteriorColor(String vehicleExteriorColor) {
        this.vehicleExteriorColor = vehicleExteriorColor;
    }

    public String getVehicleInteriorColor() {
        return vehicleInteriorColor;
    }

    public void setVehicleInteriorColor(String vehicleInteriorColor) {
        this.vehicleInteriorColor = vehicleInteriorColor;
    }

    public String getVehicleYom() {
        return vehicleYom;
    }

    public void setVehicleYom(String vehicleYom) {
        this.vehicleYom = vehicleYom;
    }

    public String getVehicleDescription() {
        return vehicleDescription;
    }

    public void setVehicleDescription(String vehicleDescription) {
        this.vehicleDescription = vehicleDescription;
    }

    public String getVehicleFuel() {
        return vehicleFuel;
    }

    public void setVehicleFuel(String vehicleFuel) {
        this.vehicleFuel = vehicleFuel;
    }

    public String getVehicleTransmission() {
        return vehicleTransmission;
    }

    public void setVehicleTransmission(String vehicleTransmission) {
        this.vehicleTransmission = vehicleTransmission;
    }

    public String getVehicleMileage() {
        return vehicleMileage;
    }

    public void setVehicleMileage(String vehicleMileage) {
        this.vehicleMileage = vehicleMileage;
    }

    public double getVehiclePrice() {
        return vehiclePrice;
    }

    public void setVehiclePrice(double vehiclePrice) {
        this.vehiclePrice = vehiclePrice;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReservedBy() {
        return reservedBy;
    }

    public void setReservedBy(String reservedBy) {
        this.reservedBy = reservedBy;
    }

    public Boolean getAd() {
        return isAd;
    }

    public void setAd(Boolean ad) {
        isAd = ad;
    }

    public Boolean getAdApproved() {
        return isAdApproved;
    }

    public void setAdApproved(Boolean adApproved) {
        isAdApproved = adApproved;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getUser() {
        return userid;
    }

    public void setUser(String user) {
        this.userid = user;
    }

    public String getCovrimg() {
        return covrimg;
    }

    public void setCovrimg(String covrimg) {
        this.covrimg = covrimg;
    }

    public List<Appointments> getAppointments() {
        return appointments;
    }

    public void setAppointments(List<Appointments> appointments) {
        this.appointments = appointments;
    }

    public List<String> getImageList() {
        return imageList;
    }

    public void setImageList(List<String> imageList) {
        this.imageList = imageList;
    }
}
