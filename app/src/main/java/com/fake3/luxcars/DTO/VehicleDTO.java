package com.fake3.luxcars.DTO;

public class VehicleDTO {
    private long vehicleId;
    private String status;
    private String reservedBy;
    private boolean isAd;

    public VehicleDTO(long vehicleId, String status) {
        this.vehicleId = vehicleId;
        this.status = status;
    }

    public VehicleDTO(long vehicleId, String status, String reservedBy) {
        this.vehicleId = vehicleId;
        this.status = status;
        this.reservedBy = reservedBy;
    }
}
