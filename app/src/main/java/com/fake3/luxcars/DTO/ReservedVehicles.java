package com.fake3.luxcars.DTO;

import java.util.Date;

public class ReservedVehicles {

    private Long id;
    private String vehicleMake;
    private  String vehicleModel;
    private String vehicleType;
    private String vehicleYom;
    private  String vehicleExteriorColor;
    private  Double vehiclePrice;
    private  String status;
    private String covrimg;
    private String reservedBy;
    private Date reservedDate;

    public ReservedVehicles() {
    }

    public ReservedVehicles(Long id, String vehicleMake, String vehicleModel, String vehicleType, String vehicleYom, String vehicleExteriorColor, Double vehiclePrice, String status, String covrimg, String reservedBy, Date reservedDate) {
        this.id = id;
        this.vehicleMake = vehicleMake;
        this.vehicleModel = vehicleModel;
        this.vehicleType = vehicleType;
        this.vehicleYom = vehicleYom;
        this.vehicleExteriorColor = vehicleExteriorColor;
        this.vehiclePrice = vehiclePrice;
        this.status = status;
        this.covrimg = covrimg;
        this.reservedBy = reservedBy;
        this.reservedDate = reservedDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleYom() {
        return vehicleYom;
    }

    public void setVehicleYom(String vehicleYom) {
        this.vehicleYom = vehicleYom;
    }

    public String getVehicleExteriorColor() {
        return vehicleExteriorColor;
    }

    public void setVehicleExteriorColor(String vehicleExteriorColor) {
        this.vehicleExteriorColor = vehicleExteriorColor;
    }

    public Double getVehiclePrice() {
        return vehiclePrice;
    }

    public void setVehiclePrice(Double vehiclePrice) {
        this.vehiclePrice = vehiclePrice;
    }

    public String getCovrimg() {
        return covrimg;
    }

    public void setCovrimg(String covrimg) {
        this.covrimg = covrimg;
    }

    public String getReservedBy() {
        return reservedBy;
    }

    public void setReservedBy(String reservedBy) {
        this.reservedBy = reservedBy;
    }

    public Date getReservedDate() {
        return reservedDate;
    }

    public void setReservedDate(Date reservedDate) {
        this.reservedDate = reservedDate;
    }
}
