package com.fake3.luxcars.Admin.AdminActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fake3.luxcars.Adapter.AppointmentsAdapter;
import com.fake3.luxcars.Adapter.ReservedAdapter;
import com.fake3.luxcars.Admin.AdminAdapters.ReservedAdapters;
import com.fake3.luxcars.DTO.ReservedDto;
import com.fake3.luxcars.DTO.ReservedVehicles;
import com.fake3.luxcars.Models.Vehicle;
import com.fake3.luxcars.R;
import com.fake3.luxcars.Services.RetrofitService;
import com.fake3.luxcars.Services.VehicleService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManageReservation extends AppCompatActivity {

    ImageView back;
    TextView title;
    RecyclerView recyclerView;
    AppointmentsAdapter vehicleAdapter;
    List<ReservedVehicles> reservedVehicle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_advertisement);
        back=findViewById(R.id.backicon);
        title=findViewById(R.id.title);
        recyclerView=findViewById(R.id.recycleView);
        title.setText("Manage Reservation");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                previous();
            }
        });

        recyclerView.setHasFixedSize(true);
        SharedPreferences prefs = getSharedPreferences("shared",MODE_PRIVATE);
        String email=  prefs.getString("email", "");
        String token=prefs.getString("token","");
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        reservedVehicle=new ArrayList<>();

        retrofit2.Retrofit retrofit= RetrofitService.getRetrofit();
        VehicleService vehicleService=retrofit.create(VehicleService.class);
        Call<List<ReservedVehicles>> pp=vehicleService.getAllReservedVehicles(token);
        pp.enqueue(new Callback<List<ReservedVehicles>>() {
            @Override
            public void onResponse(Call<List<ReservedVehicles>> call, Response<List<ReservedVehicles>> response) {
                reservedVehicle = response.body();
                ReservedAdapters reservedAdapter=new ReservedAdapters(getApplicationContext(),reservedVehicle);
                recyclerView.setAdapter(reservedAdapter);
            }
            @Override
            public void onFailure(Call<List<ReservedVehicles>> call, Throwable t) {

            }
        });



    }
    public void previous(){
        super.onBackPressed();
    }
}
