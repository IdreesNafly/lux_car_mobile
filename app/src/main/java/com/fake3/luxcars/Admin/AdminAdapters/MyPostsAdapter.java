package com.fake3.luxcars.Admin.AdminAdapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.fake3.luxcars.Models.Vehicle;
import com.fake3.luxcars.R;
import com.fake3.luxcars.Services.RetrofitService;
import com.fake3.luxcars.Services.VehicleService;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyPostsAdapter  extends RecyclerView.Adapter<MyPostsAdapter.VehicleViewHolder> {


    private Context context;
    private List<Vehicle> pendingPost;

    public MyPostsAdapter(Context context, List<Vehicle> pendingPost) {
        this.context = context;
        this.pendingPost = pendingPost;
    }
    @NonNull
    @Override
    public MyPostsAdapter.VehicleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.adapter_my_posts,parent,false);

        MyPostsAdapter.VehicleViewHolder holder=new MyPostsAdapter.VehicleViewHolder(view);
        return  holder;
    }

    @Override
    public void onBindViewHolder(@NonNull  MyPostsAdapter.VehicleViewHolder holder, final int position) {
        final Vehicle vehicle = pendingPost.get(position);
//        if(appointment.getStatus().equals("")){
//            holder.cancel.setVisibility(View.GONE);
//        }
        holder.km.setText(vehicle.getVehicleMileage());
        holder.fuel.setText(vehicle.getVehicleFuel());
        holder.interiorColour.setText(vehicle.getVehicleExteriorColor());
        holder.Auto.setText(vehicle.getVehicleTransmission());
        holder.price.setText(String.valueOf(vehicle.getVehiclePrice()));
        holder.type.setText(vehicle.getVehicleType());
        holder.status.setText(vehicle.getStatus());
        if(vehicle.getStatus().equals("Reserved")||vehicle.getStatus().equals("Sold")){
            holder.remove.setVisibility(View.GONE);
        }
        holder.make.setText(vehicle.getVehicleMake()+" "+
                vehicle.getVehicleModel()+" "+
                vehicle.getVehicleYom());
        SharedPreferences prefs = context.getSharedPreferences("shared",Context.MODE_PRIVATE);
        final String token=prefs.getString("token","");
        if(
                vehicle.getCovrimg()==null
        ){
            Picasso.get().load("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTs4gAhdje5HbqJ9yVq9O1LSnf7ui6Dh1iG9SMX3u6ZLFSAc4caxw&s").into(holder.image);
        }
        else {
            String image=vehicle.getCovrimg();
            int index = image.lastIndexOf('/');
            String lastString = image.substring(index +1);

            String updatedImage= RetrofitService.url()+"download/"+lastString;

            Picasso.get()
                    .load(Uri.parse(updatedImage))
                    .fit()
                    .centerCrop()
                    .into(holder.image);
        }

        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retrofit2.Retrofit retrofit = RetrofitService.getRetrofit();
                VehicleService vehicleService = retrofit.create(VehicleService.class);
                Call<Object> pp = vehicleService.deleteVehicle(vehicle.getId(),token);
                pp.enqueue(new Callback<Object>() {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        try {
                            Object Response = response.body();
                            if(Response.equals("true")){
                                pendingPost.remove(position);
                                // context.removeViewAt(position);
                                notifyItemRemoved(position);
                                notifyItemRangeChanged(position, pendingPost.size());
                                notifyDataSetChanged();
                                Toast.makeText(context, "Sucessfully Removed Post", Toast.LENGTH_SHORT).show();
                            }

                        }catch (Exception e){

                        }
                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable thtow) {
                    }
                });
            }
        });
    }


    @Override
    public int getItemCount() {
        return pendingPost.size();
    }


    class VehicleViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView price, km, type,fuel,Auto,interiorColour,make,status;
        Button  remove;
        ImageView image;

        public VehicleViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView=itemView.findViewById(R.id.card);
            price = itemView.findViewById(R.id.price);
            remove = itemView.findViewById(R.id.remove);
            image=itemView.findViewById(R.id.image);
            km = itemView.findViewById(R.id.km);
            make = itemView.findViewById(R.id.make);
            type = itemView.findViewById(R.id.type);
            status = itemView.findViewById(R.id.status);
            fuel = itemView.findViewById(R.id.fuel);
            Auto=itemView.findViewById(R.id.Auto);
            interiorColour=itemView.findViewById(R.id.interiorColour);
        }
    }
}


