package com.fake3.luxcars.Admin.AdminActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fake3.luxcars.Admin.AdminAdapters.TabAdapter;
import com.fake3.luxcars.Admin.AdminFragments.ManagePost.MyPost;
import com.fake3.luxcars.Admin.AdminFragments.ManagePost.PendingPost;
import com.fake3.luxcars.R;
import com.google.android.material.tabs.TabLayout;

public class ManagePost extends AppCompatActivity {
    ImageView back;
    TextView title;
    private TabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_post);
        back=findViewById(R.id.backicon);
        title=findViewById(R.id.title);
        title.setText("Manage Posts");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                previous();
            }
        });
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        adapter = new TabAdapter(getSupportFragmentManager());
        adapter.addFragment(new PendingPost(), "Pending Posts");
        adapter.addFragment(new MyPost(), "All Posts");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }
    public void previous(){
        super.onBackPressed();
    }
}
