package com.fake3.luxcars.Admin.AdminFragments.ManagePost;


import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fake3.luxcars.Admin.AdminAdapters.PendingAdapter;
import com.fake3.luxcars.Models.Appointments;
import com.fake3.luxcars.Models.Vehicle;
import com.fake3.luxcars.R;
import com.fake3.luxcars.Services.AppointmentService;
import com.fake3.luxcars.Services.RetrofitService;
import com.fake3.luxcars.Services.VehicleService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;


public class PendingPost extends Fragment {


    public PendingPost() {

    }
    RecyclerView recyclerView;
    List<Vehicle> pendingPost;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_pending_posts, container, false);
        recyclerView=view.findViewById(R.id.recycleView);
        recyclerView.setHasFixedSize(true);
        SharedPreferences prefs = getActivity().getSharedPreferences("shared",MODE_PRIVATE);
        String email=  prefs.getString("email", "");
        String token=prefs.getString("token","");
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        pendingPost=new ArrayList<>();
        retrofit2.Retrofit retrofit= RetrofitService.getRetrofit();
        final VehicleService vehicleService=retrofit.create(VehicleService.class);
        Call<List<Vehicle>> pp=vehicleService.getVehicles();
        pp.enqueue(new Callback<List<Vehicle>>() {
            @Override
            public void onResponse(Call<List<Vehicle>> call, Response<List<Vehicle>> response) {
                pendingPost = response.body();
                List<Vehicle> newPost=new ArrayList<>();
                for (Vehicle vehicle:pendingPost
                ) {
                    if(vehicle.getAd().equals(true)&&vehicle.getAdApproved().equals(false)){
                        newPost.add(vehicle);
                    }
                }
                com.fake3.luxcars.Admin.AdminAdapters.PendingPost pendingAdapter=new com.fake3.luxcars.Admin.AdminAdapters.PendingPost(getContext(),newPost);
                recyclerView.setAdapter(pendingAdapter);
            }
            @Override
            public void onFailure(Call<List<Vehicle>> call, Throwable t) {

            }
        });




        return  view;
    }

}
