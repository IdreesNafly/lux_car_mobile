package com.fake3.luxcars.Admin.AdminActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fake3.luxcars.Admin.AdminAdapters.TabAdapter;
import com.fake3.luxcars.Admin.AdminFragments.ManageVehicle.AddVehicle;
import com.fake3.luxcars.Admin.AdminFragments.ManageVehicle.ViewAllVehicle;
import com.fake3.luxcars.R;
import com.google.android.material.tabs.TabLayout;

public class ManageVehicle extends AppCompatActivity {

    ImageView back;
    TextView title;
    private TabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_vehicle);
        back=findViewById(R.id.backicon);
        title=findViewById(R.id.title);
        title.setText("Manage Vehicles");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                previous();
            }
        });
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        adapter = new TabAdapter(getSupportFragmentManager());
        adapter.addFragment(new AddVehicle(), "Add Vehicle");
        adapter.addFragment(new ViewAllVehicle(), "My Vehicles");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }
    public void previous(){
        super.onBackPressed();
    }
}
