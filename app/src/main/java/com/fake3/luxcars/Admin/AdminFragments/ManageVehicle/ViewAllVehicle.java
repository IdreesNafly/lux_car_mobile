package com.fake3.luxcars.Admin.AdminFragments.ManageVehicle;


import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fake3.luxcars.Admin.AdminAdapters.ViewAllVehiclesAdapter;
import com.fake3.luxcars.Models.Vehicle;
import com.fake3.luxcars.R;
import com.fake3.luxcars.Services.RetrofitService;
import com.fake3.luxcars.Services.VehicleService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewAllVehicle extends Fragment {
    RecyclerView recyclerView;
    List<Vehicle> vehicles;

    public ViewAllVehicle() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_view_all_vehicle, container, false);
        recyclerView=view.findViewById(R.id.recycleView);

        recyclerView.setHasFixedSize(true);
        SharedPreferences prefs = getActivity().getSharedPreferences("shared",MODE_PRIVATE);
        String email=  prefs.getString("email", "");
        String token=prefs.getString("token","");
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        vehicles=new ArrayList<>();
        retrofit2.Retrofit retrofit= RetrofitService.getRetrofit();
        final VehicleService vehicleService=retrofit.create(VehicleService.class);
        Call<List<Vehicle>> pp=vehicleService.getVehicles();
        pp.enqueue(new Callback<List<Vehicle>>() {
            @Override
            public void onResponse(Call<List<Vehicle>> call, Response<List<Vehicle>> response) {
                vehicles = response.body();
                List<Vehicle> newPost=new ArrayList<>();
                for (Vehicle vehicle:vehicles
                ) {
                    if(vehicle.getAd().equals(true)&&vehicle.getAdApproved().equals(true)){
                        newPost.add(vehicle);
                    }
                }
                ViewAllVehiclesAdapter viewAllVehiclesAdapter=new ViewAllVehiclesAdapter(getContext(),newPost);
                recyclerView.setAdapter(viewAllVehiclesAdapter);
            }
            @Override
            public void onFailure(Call<List<Vehicle>> call, Throwable t) {

            }
        });
        return  view;
    }

}
