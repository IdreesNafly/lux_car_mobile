package com.fake3.luxcars.Admin.AdminAdapters;


import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fake3.luxcars.DTO.AppointmentRequest;
import com.fake3.luxcars.Models.Appointments;
import com.fake3.luxcars.R;
import com.fake3.luxcars.Services.AppointmentService;
import com.fake3.luxcars.Services.RetrofitService;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfirmAdapter extends RecyclerView.Adapter<ConfirmAdapter.VehicleViewHolder> {


    private Context context;
    private List<Appointments> appointments;

    public ConfirmAdapter(Context context, List<Appointments> appointments) {
        this.context = context;
        this.appointments = appointments;
    }
    @NonNull
    @Override
    public ConfirmAdapter.VehicleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.adapter_confirm,parent,false);

        ConfirmAdapter.VehicleViewHolder holder=new ConfirmAdapter.VehicleViewHolder(view);
        return  holder;
    }

    @Override
    public void onBindViewHolder(@NonNull  ConfirmAdapter.VehicleViewHolder holder, final int position) {
        final Appointments appointment = appointments.get(position);
//        if(appointment.getStatus().equals("")){
//            holder.cancel.setVisibility(View.GONE);
//        }
        holder.messsage.setText(appointment.getMessage());
       // if(!appointment.getCreatedDate().toString().isEmpty()){
            //   holder.cretedDate.setText(appointment.getCreatedDate().toString());
        //}

        holder.appointmentDate.setText(appointment.getAppointmentDateTime().toString());
        holder.email.setText(appointment.getUserId());
        holder.vehicle.setText(String.valueOf(appointment.getVehicleId()));


    }



    @Override
    public int getItemCount() {
        return appointments.size();
    }


    class VehicleViewHolder extends RecyclerView.ViewHolder {

        TextView messsage, cretedDate, appointmentDate,email,vehicle;

        public VehicleViewHolder(@NonNull View itemView) {
            super(itemView);
            email = itemView.findViewById(R.id.email);
            messsage = itemView.findViewById(R.id.message);
            cretedDate = itemView.findViewById(R.id.createdDate);
            appointmentDate = itemView.findViewById(R.id.appointmentDate);
            vehicle=itemView.findViewById(R.id.vehicle);

        }
    }
}


