package com.fake3.luxcars.Admin.AdminAdapters;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.fake3.luxcars.Activities.DetailProduct;
import com.fake3.luxcars.DTO.ReservedDto;
import com.fake3.luxcars.DTO.ReservedVehicles;
import com.fake3.luxcars.DTO.VehicleDTO;
import com.fake3.luxcars.Models.Appointments;
import com.fake3.luxcars.Models.Vehicle;
import com.fake3.luxcars.R;
import com.fake3.luxcars.Services.RetrofitService;
import com.fake3.luxcars.Services.SaveListService;
import com.fake3.luxcars.Services.VehicleService;
import com.squareup.picasso.Picasso;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReservedAdapters extends RecyclerView.Adapter<ReservedAdapters.VehicleViewHolder> {


    private Context context;
    private List<ReservedVehicles> reservedVehicle;

    public ReservedAdapters(Context context, List<ReservedVehicles> reservedVehicle) {
        this.context = context;
        this.reservedVehicle = reservedVehicle;
    }
    @NonNull
    @Override
    public ReservedAdapters.VehicleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.adapter_reserved_admin,parent,false);
        ReservedAdapters.VehicleViewHolder holder=new ReservedAdapters.VehicleViewHolder(view);
        return  holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ReservedAdapters.VehicleViewHolder holder, final int position) {
        final ReservedVehicles vehicle = reservedVehicle.get(position);
        holder.make.setText(vehicle.getVehicleMake());
        holder.model.setText(vehicle.getVehicleModel());
        holder.year.setText(vehicle.getVehicleYom());
       holder.price.setText("RS :"+String.valueOf(vehicle.getVehiclePrice()));

//if(vehicle.getReservedDate().toString()!=null){
//    holder.color.setText(vehicle.getReservedDate().toString());
//}

//

        holder.status.setText(vehicle.getStatus());

        if(
                vehicle.getCovrimg()==null
        ){

            Picasso.get().load("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTs4gAhdje5HbqJ9yVq9O1LSnf7ui6Dh1iG9SMX3u6ZLFSAc4caxw&s").into(holder.imageView);
        }
        else {
            String image=vehicle.getCovrimg();
            int index = image.lastIndexOf('/');
            final String lastString = image.substring(index +1);
            String updatedImage= RetrofitService.url()+"download/"+lastString;

            Picasso.get()
                    .load(Uri.parse(updatedImage))
                    .fit()
                    .centerCrop()
                    .into(holder.imageView);
        }

        holder.viewVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent((context), DetailProduct.class);
                intent.putExtra("id",vehicle.getId());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences prefs =  context.getSharedPreferences("shared",Context.MODE_PRIVATE);
                String email=  prefs.getString("email", "");
                String token=prefs.getString("token","");
                retrofit2.Retrofit retrofit = RetrofitService.getRetrofit();
                VehicleService vehicleService = retrofit.create(VehicleService.class);
                VehicleDTO vehicleDTO=new VehicleDTO(vehicle.getId(), "Reserved", email);
                Call<Object> pp = vehicleService.cancelReservation(vehicleDTO,token);
                pp.enqueue(new Callback<Object>() {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        try {
                            Object j = response.body();
                            if (j.equals(true) ) {
                                reservedVehicle.remove(position);
                                //  context.removeViewAt(position);
                                notifyItemRemoved(position);
                                notifyItemRangeChanged(position, reservedVehicle.size());
                                notifyDataSetChanged();
                                Toast.makeText(context, "Sucessfully Cancelled Reservation", Toast.LENGTH_SHORT).show();
                            }
                            else {
                                Toast.makeText(context, "Error Removing", Toast.LENGTH_SHORT).show();
                            }
                        }catch (Exception e){
                        }
                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable thtow) {
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return reservedVehicle.size();
    }

    class VehicleViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView;
        TextView make,model,year,price,color,status;
        CardView viewVehicle;
        Button delete;

        public VehicleViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView =itemView.findViewById(R.id.image);
            make=itemView.findViewById(R.id.make);
            viewVehicle=itemView.findViewById(R.id.card);
            model=itemView.findViewById(R.id.model);
            year=itemView.findViewById(R.id.year);
            price=itemView.findViewById(R.id.price);
            color=itemView.findViewById(R.id.reservedBy);
            status=itemView.findViewById(R.id.status);
            delete=itemView.findViewById(R.id.delete);
        }
    }
}
