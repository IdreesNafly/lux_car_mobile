package com.fake3.luxcars.Admin.AdminAdapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.fake3.luxcars.Activities.DetailProduct;
import com.fake3.luxcars.Admin.AdminFragments.ManageVehicle.ViewAllVehicle;
import com.fake3.luxcars.DTO.AppointmentDTO;
import com.fake3.luxcars.DTO.VehicleRequest;
import com.fake3.luxcars.Models.Vehicle;
import com.fake3.luxcars.R;
import com.fake3.luxcars.Services.AppointmentService;
import com.fake3.luxcars.Services.RetrofitService;
import com.fake3.luxcars.Services.VehicleService;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ViewAllVehiclesAdapter extends RecyclerView.Adapter<ViewAllVehiclesAdapter.VehicleViewHolder> {

    private Context context;
    private List<Vehicle> pendingPost;

    public ViewAllVehiclesAdapter(Context context, List<Vehicle> pendingPost) {
        this.context = context;
        this.pendingPost = pendingPost;
    }
    @NonNull
    @Override
    public ViewAllVehiclesAdapter.VehicleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.adapter_view_all_vehicles,parent,false);

        ViewAllVehiclesAdapter.VehicleViewHolder holder=new ViewAllVehiclesAdapter.VehicleViewHolder(view);
        return  holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewAllVehiclesAdapter.VehicleViewHolder holder, final int position) {
        final Vehicle vehicle = pendingPost.get(position);

        holder.km.setText(vehicle.getVehicleMileage());
        holder.fuel.setText(vehicle.getVehicleFuel());
        holder.interiorColour.setText(vehicle.getVehicleExteriorColor());
        holder.Auto.setText(vehicle.getVehicleTransmission());
        holder.price.setText(String.valueOf(vehicle.getVehiclePrice()));
        holder.type.setText(vehicle.getVehicleType());
        holder.make.setText(vehicle.getVehicleMake()+" "+
                vehicle.getVehicleModel()+" "+
                vehicle.getVehicleYom());
        SharedPreferences prefs = context.getSharedPreferences("shared",Context.MODE_PRIVATE);
        final String token=prefs.getString("token","");
        if(!vehicle.getStatus().equals("Available")) {
            holder.update.setEnabled(false);
        }
        if(!vehicle.getStatus().equals("Available")) {
            holder.delete.setEnabled(false);
        }
            holder.update.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                        final AlertDialog dialogBuilder = new AlertDialog.Builder(context).create();
                        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View dialogView = inflater.inflate(R.layout.update_vehicle, null);

                        final EditText editText = (EditText) dialogView.findViewById(R.id.edt_comment);
                        Button button1 = (Button) dialogView.findViewById(R.id.buttonSubmit);
                        Button button2 = (Button) dialogView.findViewById(R.id.buttonCancel);

                        button2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialogBuilder.dismiss();
                            }
                        });
                        button1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // DO SOMETHINGS

                                VehicleRequest vehicleRequest = new VehicleRequest(vehicle.getId(), Double.valueOf(editText.getText().toString()));
                                retrofit2.Retrofit retrofit = RetrofitService.getRetrofit();
                                VehicleService vehicleService = retrofit.create(VehicleService.class);
                                Call<Object> pp = vehicleService.updateVehiclePrice(vehicleRequest, token);
                                pp.enqueue(new Callback<Object>() {
                                    @Override
                                    public void onResponse(Call<Object> call, Response<Object> response) {
                                        try {
                                            Object Response = response.body();
                                            if (Response.equals(true)) {
                                                notifyItemChanged(position);
                                                Toast.makeText(context, "Sucessfully Updated Vehicle Price", Toast.LENGTH_SHORT).show();
                                            }
                                        } catch (Exception e) {
                                            Toast.makeText(context, "Cannot Update Vehicle Price", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<Object> call, Throwable thtow) {
                                    }
                                });

                                dialogBuilder.dismiss();
                            }
                        });

                        dialogBuilder.setView(dialogView);
                        dialogBuilder.show();
                    }


            });

        if(
                vehicle.getCovrimg()==null
        ){
            Picasso.get().load("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTs4gAhdje5HbqJ9yVq9O1LSnf7ui6Dh1iG9SMX3u6ZLFSAc4caxw&s").into(holder.imageView);
        }
        else {
            String image=vehicle.getCovrimg();
            int index = image.lastIndexOf('/');
            String lastString = image.substring(index +1);

            String updatedImage= RetrofitService.url()+"download/"+lastString;

            Picasso.get()
                    .load(Uri.parse(updatedImage))
                    .fit()
                    .centerCrop()
                    .into(holder.imageView);
        }


        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retrofit2.Retrofit retrofit = RetrofitService.getRetrofit();
                VehicleService vehicleService = retrofit.create(VehicleService.class);
                Call<Object> pp = vehicleService.deleteVehicle(vehicle.getId(),token);
                pp.enqueue(new Callback<Object>() {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        try {
                            Object Response = response.body();
                            if(Response.equals(true)){
                                pendingPost.remove(position);
                                // context.removeViewAt(position);
                                notifyItemRemoved(position);
                                notifyItemRangeChanged(position, pendingPost.size());
                                notifyDataSetChanged();
                                Toast.makeText(context, "Sucessfully Deleted Vehicle", Toast.LENGTH_SHORT).show();
                            }

                        }catch (Exception e){
                            Toast.makeText(context, "Cannot Delete Vehicle", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable thtow) {
                    }
                });
            }
        });
    }


    @Override
    public int getItemCount() {
        return pendingPost.size();
    }


    class VehicleViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView price, km, type,fuel,Auto,interiorColour,make;
        Button update, delete;
        ImageView imageView;


        public VehicleViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView=itemView.findViewById(R.id.card);
            price = itemView.findViewById(R.id.price);
            update = itemView.findViewById(R.id.update);
            delete = itemView.findViewById(R.id.delete);
            km = itemView.findViewById(R.id.km);
            type = itemView.findViewById(R.id.type);
            fuel = itemView.findViewById(R.id.fuel);
            imageView=itemView.findViewById(R.id.image);
            Auto=itemView.findViewById(R.id.Auto);
            make=itemView.findViewById(R.id.make);
            interiorColour=itemView.findViewById(R.id.interiorColour);
        }
    }
}


