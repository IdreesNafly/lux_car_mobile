package com.fake3.luxcars.Models;

import java.util.Date;

public class SaveList {

    private long id;

    private Vehicle vehicle;

    private String userid;

    private Date createdAt;

    private Date updatedAt;

    public SaveList(Vehicle vehicle, String userid) {
        this.vehicle = vehicle;
        this.userid = userid;
    }

    public SaveList(long id, Vehicle vehicle, String userid, Date createdAt, Date updatedAt) {
        this.id = id;
        this.vehicle = vehicle;
        this.userid = userid;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Vehicle getVehicleid() {
        return vehicle;
    }

    public void setVehicleid(Vehicle vehicleid) {
        this.vehicle = vehicleid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
