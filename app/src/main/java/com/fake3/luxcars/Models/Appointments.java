package com.fake3.luxcars.Models;

import java.util.Date;

public class Appointments {

    private long id;

    private String message;

    private Date createdDate;

    private Date appointmentDateTime;

    private String status;

    private long vehicleid;

    private String useremail;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getAppointmentDateTime() {
        return appointmentDateTime;
    }

    public void setAppointmentDateTime(Date appointmentDateTime) {
        this.appointmentDateTime = appointmentDateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getVehicleId() {
        return vehicleid;
    }

    public void setVehicleId(long vehicleId) {
        this.vehicleid = vehicleid;
    }

    public String getUserId() {
        return useremail;
    }

    public void setUserId(String userId) {
        this.useremail = userId;
    }
}
