package com.fake3.luxcars.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.sql.Date;
import java.util.List;

public class Vehicle implements Parcelable {
    private long id;

    private String vehicleMake;
    private String vehicleModel;
    private String vehicleType;
    private String vehicleExteriorColor;
    private String vehicleInteriorColor;
    private String vehicleYom;
    private String vehicleDescription;
    private String vehicleFuel;
    private String vehicleTransmission;
    private String vehicleMileage;
    private double vehiclePrice;
    private String status;
    private String reservedBy;
    private Boolean isAd;
    private Boolean isAdApproved;
    private String user;
    private String covrimg;
    List<Appointments> appointments;
    List<String> imageList;


    public Vehicle(String vehicleMake, String vehicleModel, String vehicleType, String vehicleExteriorColor, String vehicleInteriorColor, String vehicleYom, String vehicleDescription, String vehicleFuel, String vehicleTransmission, String vehicleMileage, double vehiclePrice, String user) {
        this.vehicleMake = vehicleMake;
        this.vehicleModel = vehicleModel;
        this.vehicleType = vehicleType;
        this.vehicleExteriorColor = vehicleExteriorColor;
        this.vehicleInteriorColor = vehicleInteriorColor;
        this.vehicleYom = vehicleYom;
        this.vehicleDescription = vehicleDescription;
        this.vehicleFuel = vehicleFuel;
        this.vehicleTransmission = vehicleTransmission;
        this.vehicleMileage = vehicleMileage;
        this.vehiclePrice = vehiclePrice;
        this.user = user;
    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCovrimg() {
        return covrimg;
    }

    public void setCovrimg(String covrimg) {
        this.covrimg = covrimg;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleExteriorColor() {
        return vehicleExteriorColor;
    }

    public void setVehicleExteriorColor(String vehicleExteriorColor) {
        this.vehicleExteriorColor = vehicleExteriorColor;
    }

    public String getVehicleInteriorColor() {
        return vehicleInteriorColor;
    }

    public void setVehicleInteriorColor(String vehicleInteriorColor) {
        this.vehicleInteriorColor = vehicleInteriorColor;
    }

    public String getVehicleYom() {
        return vehicleYom;
    }

    public void setVehicleYom(String vehicleYom) {
        this.vehicleYom = vehicleYom;
    }

    public String getVehicleDescription() {
        return vehicleDescription;
    }

    public void setVehicleDescription(String vehicleDescription) {
        this.vehicleDescription = vehicleDescription;
    }

    public String getVehicleFuel() {
        return vehicleFuel;
    }

    public void setVehicleFuel(String vehicleFuel) {
        this.vehicleFuel = vehicleFuel;
    }

    public String getVehicleTransmission() {
        return vehicleTransmission;
    }

    public void setVehicleTransmission(String vehicleTransmission) {
        this.vehicleTransmission = vehicleTransmission;
    }

    public String getVehicleMileage() {
        return vehicleMileage;
    }

    public void setVehicleMileage(String vehicleMileage) {
        this.vehicleMileage = vehicleMileage;
    }

    public double getVehiclePrice() {
        return vehiclePrice;
    }

    public void setVehiclePrice(double vehiclePrice) {
        this.vehiclePrice = vehiclePrice;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReservedBy() {
        return reservedBy;
    }

    public void setReservedBy(String reservedBy) {
        this.reservedBy = reservedBy;
    }

    public Boolean getAd() {
        return isAd;
    }

    public void setAd(Boolean ad) {
        isAd = ad;
    }

    public Boolean getAdApproved() {
        return isAdApproved;
    }

    public void setAdApproved(Boolean adApproved) {
        isAdApproved = adApproved;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public List<Appointments> getAppointments() {
        return appointments;
    }

    public void setAppointments(List<Appointments> appointments) {
        this.appointments = appointments;
    }


    public List<String> getVehicleImages() {
        return imageList;
    }

    public void setVehicleImages(List<String> imageList) {
        this.imageList = imageList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }
}
